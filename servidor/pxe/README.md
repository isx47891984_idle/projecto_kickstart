# PXE  
  
Para preparar la instalación por PXE necesitamos instalar syslinux, tftpd y dhcpd.  
\# yum install syslinux tftp dhcpd  
  
Modificamos el archivo /etc/xinetd.d/tftp, cambiamos el directorio de publicacion al directorio de pxe y lo activamos.  
```sh  
        server_args             = -s /servidor/pxe  
        disable                 = no  
```  
  
\# systemctl restart xinetd  
  
  
Modificamos el archivo /etc/dhcpd/dhcpd.conf y lo dejamos tal que asi.  
```sh  
    subnet 192.168.0.0 netmask 255.255.0.0 {  
            range 192.168.0.190 192.168.0.210;  
            option broadcast-address 192.168.255.255;  
            option routers 192.168.0.10;  
            option root-path "/servidor/pxe/";  
            filename "pxelinux.0";  
    }  
  
    host i06 {  
            hardware ethernet ac:22:0b:c7:db:de;  
            fixed-address 192.168.0.196; #C0A800C4  
    }  
```  
  
A partir de este punto no necesitamos estar conectados a internet, modificamos el archivo de configuracion de la interficie p5p1, cambiamos BOOTPROTO a estatica y nos ponemos la IP 192.168.0.10/16.  
/etc/sysconfig/network-scripts/ifcfg-p5p1  
```sh  
    BOOTPROTO=static  
    IPADDR=192.168.0.10  
    NETMASK=255.255.0.0  
```  
Ahora reiniciamos dhcpd y NetworkManager.  
  
\# systemctl restart dhcpd NetworkManager  
  
Ya podemos conectar los dos pcs entre si y seguir configurando.  
  
Copiamos el bootloader a /servidor/pxe.  
\# cp /usr/share/syslinux/pxelinux.0 /servidor/pxe/  
  
Creamos dos archivos en /servidor/pxe/pxelinux.cfg “default” y “C0A800C4”.  
default  
```sh  
	DEFAULT Fedora20  
	PROMPT 0  
	TIMEOUT 20  
  
	LABEL Fedora20  
	   KERNEL vmlinuz  
	   APPEND initrd=initrd.img \   
		  repo=nfs://192.168.0.10/servidor/nfs/install/fedora/20/ \  
		  ks=http://192.168.0.10/KickStartPXE.cfg \  
		  vnc vncconnect=192.168.0.10:63596  
```  
  
C0A800C4  
```sh  
	DEFAULT Fedora20  
	PROMPT 0  
	TIMEOUT 20  
	      
	LABEL Fedora20  
	   KERNEL vmlinuz  
	   APPEND initrd=initrd.img \  
		  repo=nfs://192.168.0.10/servidor/nfs/install/fedora/20/  \  
		  ks=http://192.168.0.10/i06KickStartPXE.cfg \  
		  vnc vncconnect=192.168.0.10:63596  
```  
  
