% Instalación headless con kickstart y VNC
% David Pareja Rodríguez
% 27 Mayo 2015

# Guia de Contenidos

* Instalación Headless
	+ Que es KickStart?
	+ Opciones basicas de KickStart.
	+ Ejemplo de fichero KS.

# Guia de Contenidos
* Preparación del Projecto.
	+ Partición /dev/sda2 de 100GB.
	+ Imagen iso de Fedora 20.	
	+ Configurar servicios.
	+ Modificar Grub

# Guia de Contenidos
* Objetivos
	+ Instalación desatendida del sistema operativo.
	+ Probar diferentes medios de instalación (http, ftp, nfs, pxe).	
	+ Crear ficheros KickStart diferentes segun objetivo.
	    - Instalación minima (Solo modo consola).
	    - Instalación solo Servidor.
	    - Instalación basica con escritorio.
	    - Instalación solo escritori.
	    - Instalación tipo quiosco (X + Firefox).
	    - Instalación cliente de ventanas.


# Que es KickStart?
* Metodo de instalación automatizada de RedHat (portada a otras distribuciones).  
Similar a FAI(Fully Automatic Installer) de Debian o a AutoYaST de SUSE.  
Consiste en un archivo de texto plano que contiene tres secciones (orden no importa).
	1. Opciones (Configuración)
	2. Paquetes (%packages)
	3. Scripts (%pre y %post)

# Para que quiero KickStart?
* Para automaticar instalaciones repetitivas de maquinas.
	+ Laboratorio
	+ Escuela
	+ CyberCafe
	+ Oficina

# Opciones Basicas
![](codigobasico1.png)

# Opciones Basicas
![](codigobasico2.png)

# Ejemplo de fichero KS
Ejemplo de fichero KickStart para pc i06: [Fichero KS i06](./i06KickStartPXE.cfg)  
Script Post-Instalación: [PostScript i06](./servidor/ftp/post-script-i06.sh)

# Partición /dev/sda2
* Modificar fstab
* Crear directorios
* Copiar archivos


# Imagen iso Fedora 20
* Descargar iso desde gandhi.
* Montar y copiar ficheros.

# Configurar Servicios
* FTP
* HTTP
* NFS
* PXE
	+ tftp
	+ dhcpd

# Modificar GRUB
* Añadir una entrada personalizada para FTP, HTTP y NFS.
![](entradasgrub.png)

# Instalación minima (Solo modo consola)
![](consolaKS.png)

# Instalación solo Servidor
![](servidorKS.png)

# Instalación basica con escritorio
![](basicescritoriKS.png)

# Instalación solo escritori
![](escritorioKS.png)

# Instalación tipo quiosco (X + Firefox)
![](kioskKS.png)

# Instalación cliente de ventanas
![](finestresKS.png)

# Conclusión
KickStart te quita mucho trabajo de encima y por eso...
<big>mola</big>

